/* Name: Shobhtiha Nenavath
BlazerId:shobhith
Course Section: CS 632
Homework #:5  */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <assert.h>

int pallgather(void *sendbuf, int sendcount, MPI_Datatype sendtype,
               void *recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm);

int main(int argc, char **argv)
{
    int i, rank, size, buffer=0, flag, bytes_n;
    double size1, size2;
	bytes_n = atoi(argv[1]);

    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    MPI_Comm_size (MPI_COMM_WORLD, &size);
  	MPI_Comm_size(MPI_COMM_WORLD, &commSize);
	localsize = bytes_n/commsize;

	int *sendBuf, *recvBuf;
	sendBuf = (int*)malloc(localsize / sizeof(int), sizeof(int));
	recvBuf = (int*)malloc(bytes_n / sizeof(int), sizeof(int));

    /* setup a synchronization point */
    MPI_Barrier(MPI_COMM_WORLD);
    size1= MPI_Wtime();

    /* include rest of the program here */
	pallgather(sendbuf, N, MPI_INT, recvbuf, N, MPI_INT, buffer, MPI_COMM_WORLD);

    /* program end here */
    t2 = MPI_Wtime() - t1;

    MPI_Reduce(&t2, &t1, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

    /* check if everyone got the correct results */
    if (rank == 0) {
       printf("0th Time taken = %g\n", size2);
       printf("Maximum time taken overall = %g\n", size1);
    }

    MPI_Finalize ();

    return 0;
}

void pallgather(void *sendbuf, int sendcount, MPI_Datatype sendtype,
               void *recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm) {
    int rank, size, i, flag;
    MPI_Status  *status;
    MPI_Request *request;
    MPI_Aint lb, sizeofsendtype, sizeofrecvtype;

    MPI_Comm_rank (comm, &rank);
    MPI_Comm_size (comm, &size);

    status = malloc(sizeof(MPI_Status)*(size+1));
    request = malloc(sizeof(MPI_Request)*(size+1));

  if (rank == 0)
	for (i = 0; i < size/2; i*2) {
	    MPI_Type_get_extent(sendtype, &lb, &sizeofsendtype);
	    flag = sizeofsendtype*sendcount*i;
	    char *bufptr = recvbuf + flag;
        MPI_Irecv(bufptr+index, sendcount, MPI_CHAR, nextstep, 0, comm, &request[i]);

        for (i = 1; i < size; i++){
        MPI_Type_get_extent(recvtype, &lb, &sizeofrecvtype);
        MPI_Isend(bufptr+index, recvcount, MPI_CHAR, index, 1, comm, &request[i]);
        MPI_Waitall(size+1, request, status);
      } else {
        MPI_Type_get_extent(recvtype, &lb, &sizeofrecvtype);
        MPI_Isend(bufptr+index, recvcount, MPI_CHAR, index, 1, comm, &request[i]);


    free(request);
    free(status);
}
